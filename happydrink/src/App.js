import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { establishments } from './establishments/fixtures'
import { Establishment } from './establishments/establishment'

class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            pseudo: "Inconnu",
            searchStringUser: ''
        }
        // ! On utilise plus de bind avec ES6, l'utilisation 
        // ! d'une fonction flèchée permet de contourner ce problème.
        // this.handleChange(e) = this.handleChange.bind(this)
    }

    handleChange = (e) => {
        this.setState({searchStringUser: e.target.value})
    }

    randomPseudo = () => {
        let randomPseudo = ""
        const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        const size = Math.floor(Math.random() * 10) + 5
        for( let i=0; i < size; i++) {
            randomPseudo += chars.charAt(Math.floor(Math.random() * chars.length))
        }

        this.setState({
            pseudo: randomPseudo
        })
    }


    render(){
        const establishmentFilter = establishments.filter((searchText) => {
            let search = searchText.name + " " + searchText.description;
            return search.toLowerCase().match(this.state.searchStringUser)
        })

        const listEstablishments = establishmentFilter.map( (establishment) => {
            return (
                <Establishment 
                    key={establishment.id} 
                    establishment={establishment} //L'élément de l'objet sera envoyé en tant que this.props.establishmet dans son component
                />
                // ? L'attribut key permet à React d'identifier les éléments
                // ? et d'améliorer les performances lors de l'ajout
            )
        })

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Hi, {this.state.pseudo} and welcome to { this.props.title }</h2>
                    <p> <button onClick={this.randomPseudo}>Changer le pseudo</button></p>
                    <div>
                        <input
                            type="text"
                            placeholder="search"
                            value={this.state.searchStringUser}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="App-intro">
                        { listEstablishments }
                    </div>
                </header>
            </div>
        );
    }
}

export default App;
