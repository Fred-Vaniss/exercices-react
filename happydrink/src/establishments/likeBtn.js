import React from 'react'

export default class LikeBtn extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            likes           : this.props.likes,
            liked           : 0,
            fav             : false,
            favStyle        : '',
            likeStyle       : '',
            dislikeStyle    : '',
        }
    }

    styleBtn = (operation) => {
        let likeStyle = this.state.likeStyle
        let dislikeStyle = this.state.dislikeStyle

        switch (operation) {
            case 'like':
                likeStyle = 'btn-on'
                dislikeStyle = ''
                break;

            case 'dislike':
                likeStyle = ''
                dislikeStyle = 'btn-on'
                break;

            case 'none':
                likeStyle = ''
                dislikeStyle = ''
                break;
        
            default:
                break;
        }

        this.setState({
            likeStyle: likeStyle,
            dislikeStyle: dislikeStyle,
        })
    }

    like = (operation) => {
        let likes = this.props.likes
        let liked = this.state.liked

        switch (operation) {
            case 'plus':
                if (liked <= 0){
                    liked = 1
                    this.styleBtn('like')
                } else {
                    liked = 0
                    this.styleBtn('none')
                }
                break;

            case 'minus':
                if (liked >= 0){
                    liked = -1
                    this.styleBtn('dislike')
                } else {
                    liked = 0
                    this.styleBtn('none')
                }
                break;
            
            default:
                break;
        }

        likes = likes + liked

        this.setState({
            likes: likes,
            liked: liked
        })
    }

    toggleFavorite = () => {
        if (!this.state.fav) {
            this.setState({fav: true, favStyle: 'btn-on'})
            console.log("favorited")
        } else {
            this.setState({fav: false, favStyle: ''})
            console.log("unfavorited")
        }
    }

    render(){
        return(
            <div>
                <button onClick={() => this.like('plus')} className={"vote-btn " + this.state.likeStyle}>
                    <span role="img" aria-label="Like">👍</span>
                </button> 
                {this.state.likes} 
                <button onClick={() => this.like('minus')} className={"vote-btn " + this.state.dislikeStyle}>
                    <span role="img" aria-label="Dislike">👎</span>
                </button>
                <button onClick={this.toggleFavorite} className={"vote-btn fav " + this.state.favStyle}>
                    <span role="img" aria-label="Favorite">⭐</span>
                </button>
            </div>
        )
    }
}