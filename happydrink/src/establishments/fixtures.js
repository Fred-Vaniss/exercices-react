export const establishments = [
    {
        id          : "0890786GH",
        name        : "Biberium",
        description : "Un super bar karaoké",
        likes       : 106
    },
    {
        id          : "0890786GD",
        name        : "Brew Dogs",
        description : "Un super bar à bière",
        likes       : 18
    },
    {
        id          : "MJLMH0389",
        name        : "Batonier",
        description : "Un super bar de fin de soirée",
        likes       : 46
    },
    {
        id          : "HB9RNZAC8",
        name        : "Soulstorm",
        description : "Un bar médiocre avec de la bière qui à un mauvais goût",
        likes       : -89
    }
] 