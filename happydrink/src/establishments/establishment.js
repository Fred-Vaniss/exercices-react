import React from 'react';
import LikeBtn from './likeBtn';

export class Establishment extends React.Component{
    render(){
        return(
            <div className="establishment">
                <h3>{ this.props.establishment.name }</h3>
                { this.props.establishment.description }
                <LikeBtn likes={this.props.establishment.likes}/>
            </div>
        )
    }
}