import React from 'react'

import './GuessCount.css'

const GuessCount = ({ guesses }) => <div className="guesses">Cartes trouvés: {guesses}</div>

export default GuessCount
