// * Application principale
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


ReactDOM.render(<App />, document.getElementById('root'));


// * Horloge en temps réel
// ? Pour que l'horloge change chaque secondes, il à fallu à chaque intervale, refaire un render avec la nouvelle heure
// import React from 'react';
// import ReactDOM from 'react-dom';
// setInterval(() => {
//     const timer = (
//         <div>
//             <h2>Il est exactement {new Date().toLocaleTimeString()}.</h2>
//         </div>
//     );
//     ReactDOM.render(timer, document.getElementById('root'))
// }, 1000);

// * Horloge en temps réel via une classe avec un State
// ? Avec le setState(), le render détecte les changements et actualise le nouveau contenu sans devoir faire
// ? appel à un nouveau render.
// import React, { Component, Fragment } from 'react'
// import ReactDOM from 'react-dom'
// class Composant extends Component{
//     constructor(props){
//         super(props)
//         this.state = {
//             time: new Date().toLocaleTimeString(),
//             date: new Date().toLocaleDateString()
//         }

//         setInterval(() => {
//             this.setState({
//                 time: new Date().toLocaleTimeString(),
//                 date: new Date().toLocaleDateString()
//             })
//         }, 1000);
//     }

//     render(){
//         return (
//             // ! L'utilisation de Fragment est important si on veut mettre plusieurs éléments séparés et éviter une erreur
//             // ! (Ne pas oublier de mettre {Fragment} dans l'import de React)
//             <Fragment>
//                 <div>{ this.state.date }</div>
//                 <div>{ this.state.time }</div>
//             </Fragment>
//         );
//     }
// }
// ReactDOM.render(<Composant/>, document.getElementById('root'))

// * Evénement dans une classe
// import React, { Component, Fragment } from 'react';
// import ReactDOM from 'react-dom';

// class Composant extends Component {
//     constructor (props){
//         super(props)
//         this.state = {
//             compteur : 0
//         };
//     }
//     additionner() {
//         this.setState({
//             compteur: this.state.compteur + 1
//         })
//     }

//     render(){
//         return (
//             <Fragment>
//                 <div>
//                     <span>{ this.state.compteur }</span>
//                     <div>
//                         <button onClick={() => this.additionner()}>+1</button>
//                     </div>
//                 </div>
//             </Fragment>
//         )
//     }
// }

// ReactDOM.render(<Composant/>, document.getElementById('root'))