import React from 'react';
import { Header, FilmList, FilmDetail} from './components'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <div className="App">
      <div className="row">
        <Header />
      </div>
      <div className='d-flex flex-row'>
        <FilmList />
        <FilmDetail />
      </div>

    </div>
  );
}

export default App;