import React from 'react'

export default class FilmElement extends React.Component{
    render(){
        return (
            <div className="w-25 p-3">
                <div class="card"   >
                    <img src="https://image.tmdb.org/t/p/w600_and_h900_bestv2/1ePsCN6vpduAX1LxEdqfbwfS4nf.jpg" class="card-img-top" alt="..."/>
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="/" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        )
    }
}