import React from 'react';
import {FilmElement} from './..'

export default class FilmList extends React.Component {
    render () {
        return (
            <div className="col-sm-9 d-flex flex-row flex-wrap align-content-start">
                <FilmElement/>
                <FilmElement/>
                <FilmElement/>
                <FilmElement/>
            </div>
        )
    }
}