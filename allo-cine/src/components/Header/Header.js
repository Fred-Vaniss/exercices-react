import React from 'react';

// * Première manière de créer la classe: on crée et exporte la classe en une seule ligne.
export default class Header extends React.Component{
    render(){
        return (
            <header className="col navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <a className="navbar-brand" href="/">Kill AlloCiné</a>
                    <button className="navbar-toggler">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active">
                        <a className="nav-link" href="/">Home</a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" href="/">Favoris</a>
                        </li>
                    </ul>
                    </div>
                </div>
            </header>
        );
    }
}

// * Deuxième manière de créer la classe: on crée la classe et seulement après on exporte
// ! Dans ce cas il faut ajouter "{ Component }" dans l'import
// import React, { Component } from 'react';
// class Header extends Component{

// }

// export default Header;