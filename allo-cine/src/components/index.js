// * Identification de chaque component
// ? Ca va nous épargner plusieurs lignes d'import sur app.js
export { default as Header } from './Header/Header'
export { default as FilmDetail} from './FilmDetail/FilmDetail'
export { default as FilmElement} from './FilmDetail/FilmElement'
export { default as FilmList} from './FilmList/FilmList'