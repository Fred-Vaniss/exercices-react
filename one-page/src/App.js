import React, { Fragment } from 'react';
import './App.css';
import { Header, One, Two, Three, Four, Footer } from './Components'

function App() {
  return (
    <Fragment>
      <Header/>
      <One/>
      <Two/>
      <Three/>
      <Four/>
      <Footer/>
    </Fragment>
  );
}

export default App;
