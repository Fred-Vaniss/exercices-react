// * Exportation de chaque component.
// ? Cette méthode rend l'exportation plus rapide avec moins de ligne de code dans App.js
// ? L'import dans App.js donne: import { Header, One, Two, Three, Four, Footer } from './Components'

export {default as Header} from './Sections/header'
export {default as One} from './Sections/one'
export {default as Two} from './Sections/two'
export {default as Three} from './Sections/three'
export {default as Four} from './Sections/four'
export {default as Footer} from './Sections/footer'