import  React, { Component } from 'react';
import Gnoufs from './Gnoufs';
import Message from './Message';

export default class Welcome extends Component {
    render () {
        return (
            <div className="App">
                <Gnoufs/>
                <Message/>
            </div>
        )
    }
}