import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// import App from './App';


// ReactDOM.render(<App/>, document.getElementById('root'));

// * Anciennce méthode pour créer un élément HTML
// const titre = React.createElement(
//     'h1', 'title', 'Hello World'
// );
// ReactDOM.render(para, document.getElementById('root'))

// * Nouvelle méthode en JSX
// ? Ce sera la méthode la plus utilisée et la plus pratique
// const titre = <h1 className="title">Hello world</h1>
// const para = 
//     <div>
//         <h1>Hello world!</h1>
//         <p>C'est tellement plus pratique!</p>
//     </div>

// ReactDOM.render(titre, document.getElementById('root'));

// * Les "Components" constituent les différentes pièces de notre application (ou les briques d'une maison)

// * Création d'un Component par une fonction
// ? Mettre une majustule à la fonction et l'appel se fait dans une balise fermée < />
// const Fonction = () => {
//     return <h1>Hello there</h1>
// }
// ReactDOM.render(<Fonction/>, document.getElementById('root'))


// * Création d'un Component par une classe es6
// ? La création d'un Component sous forme de classe impose l'utilisation
// ? de la fonction render() pour qu'il puisse s'afficher
// class Fonction extends React.Component{
//     render(){
//         return <h1>Hello there!</h1>
//     }
// }
// ReactDOM.render(<Fonction/>, document.getElementById('root'))


// * Les "props" permet de passer des paramètres à nos components
// ? Les paramètres sont passés via les attributs HTML lors de l'appel du component

// * Des props dans une classe
// class Fonction extends React.Component{
//     // constructor (props){
//     //     super(props)
//     // }
//     render(){
//         return <h1>Salut, tu dois t'appeller { this.props.nom } { this.props.prenom }</h1>
//     }
// }
// ReactDOM.render(<Fonction nom="Vaniss" prenom="Fred"/>, document.getElementById('root'));

// * Des props dans une fonction
// const Fonction = (props) => {
//     return <h1>Salut, tu dois t'appeller { props.nom } { props.prenom }</h1>
// }
// ReactDOM.render(<Fonction nom="Vaniss" prenom="Fred"/>, document.getElementById('root'))


// * Utilisation de plusieurs components
// ? Ici on crée trois composants: Photo, Nom et Bio, on les mets tous dans le composant Profil qui servira à appeler les trois en même temps dans une div.
// const Photo = () => <img style={{height: '200px'}} src="https://aftercoaching.be/wp-content/uploads/2018/08/PHOTO-Guy_Vilain_TEMPORAIRE.png" alt=""/>;

// const Nom = () => <h4>Guy Vilain</h4>;

// const Bio = () =>
//     <p>
//         <strong>Biographie </strong>
//         Guy est un type sympa, et il ne manque pas de classe.
//     </p>;

// const Profil = () => 
//     <div>
//         <Photo/>
//         <Nom/>
//         <Bio/>
//     </div>;

// ReactDOM.render(<Profil/>, document.getElementById('root'));